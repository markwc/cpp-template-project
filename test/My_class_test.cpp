#include <MyClass.h>
#include <gtest/gtest.h>

#include <array>
#include <random>

namespace {

class MyClass_test : public ::testing::Test {
 public:
  MyClass_test() = default;

  ~MyClass_test() override = default;

  MyClass_test(MyClass_test &&) = delete;

  MyClass_test(const MyClass_test &) = delete;

  MyClass_test &operator=(MyClass_test &&) = delete;

  MyClass_test &operator=(const MyClass_test &) = delete;

 protected:
  void SetUp() override {}

  void TearDown() override {}
};

TEST_F(MyClass_test, instantiate) {
  Katas::MyClass UUT;
  ASSERT_TRUE(true);
}

TEST_F(MyClass_test, create) {
  auto p_UUT = std::make_unique<Katas::MyClass>();
  ASSERT_NE(nullptr, p_UUT);
}
}  // namespace