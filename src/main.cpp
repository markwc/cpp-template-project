/// @file main.cpp

#include <MyClass.h>

#include <iostream>

int main() {
  std::cout << "Main run" << std::endl;
  return 0;
}