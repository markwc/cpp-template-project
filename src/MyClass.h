// MyClass.h
#ifndef MYCLASS_H
#define MYCLASS_H

#include <memory> // For std::unique_ptr

namespace Katas {
class MyClass {
public:
  MyClass();  // Constructor
  ~MyClass(); // Destructor
  // Rule of Five
  MyClass(const MyClass &other);                // Copy constructor
  MyClass(MyClass &&other) noexcept;            // Move constructor
  MyClass &operator=(const MyClass &other);     // Copy assignment operator
  MyClass &operator=(MyClass &&other) noexcept; // Move assignment operator

  void doSomething(); // Example public method

private:
  class Impl;                 // Forward declaration of the implementation class
  std::unique_ptr<Impl> impl; // Unique pointer to the hidden implementation
};

} // namespace Katas
#endif // MYCLASS_H
