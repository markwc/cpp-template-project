// MyClass.cpp
#include "MyClass.h"

namespace Katas {
// Implementation class defined inside the .cpp file
class MyClass::Impl {
public:
  void doSomethingImpl() {
    // Actual implementation of doSomething
  }
};

MyClass::MyClass() : impl(std::make_unique<Impl>()) {}

MyClass::~MyClass() = default; // Defaulted to use the unique_ptr's destructor

// Copy constructor
MyClass::MyClass(const MyClass &other) : impl(std::make_unique<Impl>(*other.impl)) {}

// Move constructor
MyClass::MyClass(MyClass &&other) noexcept = default;

// Copy assignment operator
MyClass &MyClass::operator=(const MyClass &other) {
  if (this != &other) {  // Self-assignment check
    *impl = *other.impl; // Deep copy of the implementation
  }
  return *this;
}

// Move assignment operator
MyClass &MyClass::operator=(MyClass &&other) noexcept = default;

void MyClass::doSomething() {
  impl->doSomethingImpl(); // Delegate the call to the implementation
}

} // namespace Katas
